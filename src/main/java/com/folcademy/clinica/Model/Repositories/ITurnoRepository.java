package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Entities.Turno;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ITurnoRepository extends JpaRepository<Turno,Integer> {
    List<Turno> findAll();
    Optional<Turno> findById(Integer id);
}
