package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IMedicoRepository extends JpaRepository<Medico,Integer> {
    List<Medico> findByNombreLike(String nombre);
    Optional<Medico> findById(Integer id);
    Optional<Medico> findByNombreAndApellido(String nombre, String apellido);
 }
