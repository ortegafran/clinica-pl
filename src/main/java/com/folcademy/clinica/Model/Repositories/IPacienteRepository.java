package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IPacienteRepository extends JpaRepository<Paciente, Integer> {
    List<Paciente> findAllByApellido(String apellido);
    Optional<Paciente> findById(Integer id);
    Optional<Paciente> findByDni(String dni);
    Optional<Paciente> findByNombreLikeAndApellidoLike(String nombre, String apellido);

}
