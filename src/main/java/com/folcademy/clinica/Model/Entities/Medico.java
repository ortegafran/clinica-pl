package com.folcademy.clinica.Model.Entities;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="medico")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Medico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idmedico",columnDefinition = "INTEGER")
    public Integer id;
    @Column(name = "nombre", columnDefinition = "VARCHAR")
    public String nombre;
    @Column(name = "apellido", columnDefinition = "VARCHAR",nullable = false)
    public String apellido;
    @Column(name = "profesion", columnDefinition = "VARCHAR",nullable = false)
    public String profesion;
    @Column(name = "consulta", columnDefinition = "INTEGER",nullable = false)
    public Integer consulta;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Medico medico = (Medico) o;
        return id != null && Objects.equals(id, medico.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
