package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TurnoMapper {
    private final MedicoMapper medicoMapper;
    private final PacienteMapper pacienteMapper;

    public TurnoMapper(MedicoMapper medicoMapper, PacienteMapper pacienteMapper) {
        this.medicoMapper = medicoMapper;
        this.pacienteMapper = pacienteMapper;
    }

    public TurnoDto entityToDto(Turno entity){
        return Optional.ofNullable(entity)
                .map(e-> new TurnoDto(
                        e.getId(),
                        medicoMapper.entityToDto(e.getMedico()),
                        pacienteMapper.entityDto(e.getPaciente()),
                        e.getFecha(),
                        e.getHora(),
                        e.getAtendido()
                )).orElse(new TurnoDto());
    }

    public Turno dtoToEntity(TurnoDto dto){
        System.out.println(dto);
        Turno turno = new Turno();
        turno.setId(dto.getId());
        turno.setAtendido(dto.getAtendido());
        turno.setFecha(dto.getFecha());
        turno.setHora(dto.getHora());

        turno.setMedico(medicoMapper.dtoToEntity(dto.getMedico()));
        turno.setPaciente(pacienteMapper.dtoToEntity(dto.getPaciente()));

        return turno;
    }

}
