package com.folcademy.clinica.Services;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.IMedicoRepository;
import com.folcademy.clinica.Services.Interfaces.IMedicoService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicoService implements IMedicoService {
    private final IMedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;

    public MedicoService(IMedicoRepository medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }

    @Override
    public List<MedicoDto> findAll() {
        return medicoRepository
                .findAll()
                .stream()
                .map(medicoMapper::entityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public MedicoDto findById(Integer id) {
        return medicoRepository
                .findById(id)
                .map(medicoMapper::entityToDto)
                .orElse(null);
    }

    public MedicoDto save(MedicoDto dto){
        return medicoMapper.entityToDto(
                            medicoRepository.save(
                                    medicoMapper.dtoToEntity(dto)
                            )
                        );
    }

    public MedicoDto edit(MedicoDto dto){
        if(!medicoRepository.findById(dto.getId()).isPresent())
            return null;

        return medicoMapper.entityToDto(
                medicoRepository.save(
                        medicoMapper.dtoToEntity(dto)
                )
        );
    }

    public MedicoDto delete(Integer id){
        Optional<Medico> deleted = medicoRepository.findById(id);
        if(!deleted.isPresent())
            return new MedicoDto();

        medicoRepository.delete(deleted.get());

        return medicoMapper.entityToDto(deleted.get());
    }

    public MedicoDto findByNombre(String nombre){
        List<Medico> m = medicoRepository.findByNombreLike(nombre);
        for (Medico i:m) {
            System.out.println(m);
        }
        return null;
    }

    public boolean alreadyExists(MedicoDto medico){
        Optional<Medico> result = medicoRepository.findByNombreAndApellido(medico.getNombre(),medico.getApellido());
        return result.isPresent();
    }
}
