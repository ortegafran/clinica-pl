package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.Classes.AlreadyExistsException;
import com.folcademy.clinica.Exceptions.Classes.CouldntComplete;
import com.folcademy.clinica.Exceptions.Classes.NotFoundException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.IPacienteRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PacienteService implements IPacienteService {
    private final IPacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;

    public PacienteService(IPacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }

    @Override
    public List<PacienteDto> findAll() {
        return pacienteRepository
                .findAll()
                .stream()
                .map(pacienteMapper::entityDto)
                .collect(Collectors.toList());
    }


    @Override
    public PacienteDto findById(Integer id) {

        return pacienteRepository
                .findById(id)
                .map(pacienteMapper::entityDto)
                .orElse(null);
    }

    public PacienteDto save(PacienteDto dto){

        return pacienteMapper.entityDto(
                pacienteRepository.save(
                        pacienteMapper.dtoToEntity(dto)
                )
        );
    }

    public PacienteDto edit(PacienteDto dto){
        if(!pacienteRepository.findById(dto.getId()).isPresent())
            return null;

        return pacienteMapper.entityDto(
                pacienteRepository.save(
                        pacienteMapper.dtoToEntity(dto)
                )
            );
    }

    public PacienteDto delete(Integer id){
        Optional<Paciente> paciente = pacienteRepository.findById(id);
        if(!paciente.isPresent())
            return null;

        PacienteDto p = pacienteMapper.entityDto(paciente.get());
        pacienteRepository.deleteById(p.getId());
        return p;
    }

    public PacienteDto findByDni(String dni){
        Optional<Paciente> paciente = pacienteRepository.findByDni(dni);
        if(!paciente.isPresent()) return null;
        else return pacienteMapper.entityDto(paciente.get());
    }
}
