package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.folcademy.clinica.Exceptions.Classes.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/turnos")
public class TurnosController {
    private final TurnoService turnoService;

    public TurnosController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    @GetMapping("")
    public ResponseEntity<List<TurnoDto>> findAll(){
        return ResponseEntity.ok(turnoService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TurnoDto> findOne(@PathVariable(name="id") Integer id){
        return ResponseEntity.ok(turnoService.findById(id));
    }

    @PostMapping("")
    public ResponseEntity<TurnoDto> addOne(@RequestBody @Validated TurnoDto turno){
        if(turno.getMedico()==null || turno.getMedico().getId()==null)
            throw new ValidationException("Medico no valido");

        if(turno.getPaciente()==null || turno.getPaciente().getId()==null)
            throw new ValidationException("Paciente no valido");

        TurnoDto saved = turnoService.save(turno);
        if(saved==null)
            throw new ValidationException("Error de datos.");

        return ResponseEntity.ok(saved);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<TurnoDto> editOne(@PathVariable(name="id") Integer id,
                                            @RequestBody TurnoDto t){
        t.setId(id);
        TurnoDto editted = turnoService.edit(t);
        return ResponseEntity.ok(editted);
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<TurnoDto> delete(@PathVariable(name="id") int id) {
        TurnoDto turno = turnoService.delete(id);

        if(turno==null)
            throw new NotFoundException("Turno no encontrado");

        return ResponseEntity.ok(turno);
    }
}
